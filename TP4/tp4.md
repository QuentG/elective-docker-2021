### TP4 - Création d'un docker-compose.yml

#### docker-compose

```docker
version:  '3.7'

services:
  node:
    image: node:11-alpine
    restart: on-failure
    volumes:
      - ./myDir:/srv/myDir:cached
    working_dir: /srv/myDir
    command: >
      sh -c "cd /srv/myDir
      && echo 'Write line' > text.txt"
```

