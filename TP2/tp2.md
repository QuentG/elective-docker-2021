### TP2 - Création d'un Dockerfile

#### Dockerfile

```docker
FROM alpine:latest # Image de base

ENV WORKDIR_PATH=/home

WORKDIR $WORKDIR_PATH

COPY test.txt . 

CMD [ "cat", "test.txt" ]
```

Donnez la commande qui permet de créer cette image en lui donnant un nom : 

```sh
docker build -f Dockerfile . -t quentg/testimg
```

Donnez la commande qui permet lancer un container depuis cette image : 

```sh
docker run quentg/testimg
```

