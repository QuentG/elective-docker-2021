# Elective Docker 2021

### Liste des TPS :

- [TP1 - Manipulation Docker](https://gitlab.com/QuentG/elective-docker-2021/-/blob/master/TP1/TP1.txt)
- [TP2 - Création d'un Dockerfile](https://gitlab.com/QuentG/elective-docker-2021/-/blob/master/TP2/tp2.md)
- [TP4 - Création d'un docker-compose.yml](https://gitlab.com/QuentG/elective-docker-2021/-/blob/master/TP4/tp4.md)
- [TP5 - Création d'un docker-compose.yml 2](https://gitlab.com/QuentG/elective-docker-2021/-/blob/master/TP5/tp5.md)