### TP5 - Création d'un docker-compose.yml 2

#### docker-compose

```docker
version:  '3.7'

services:
  customimg:
    image: quentg/test-image
    restart: on-failure
    volumes:
      - ./home:/home:cached
    working_dir: /home
    command: >
      sh -c "echo \"`date`\" >> /home/test.txt"
      && cat test.txt"
```

